﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadorDeSueldos
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado gerente = new Empleado(Tipo.Gerente);

            Calculadora calculadora = new Calculadora();
            Console.WriteLine("El {0} gana: ", Tipo.Gerente);
            Console.WriteLine(calculadora.ObtenerSueldo(Tipo.Gerente, 0));

            Empleado vendedorPorComision = new Empleado(Tipo.VendedorPorComision);
            Console.WriteLine("El {0} gana: ", Tipo.VendedorPorComision);
            Console.WriteLine(calculadora.ObtenerSueldo(Tipo.VendedorPorComision, 10));

        }
    }

    class Calculadora
    {
        double valorPorVenta = 1000;
        public double ObtenerSueldo(Tipo tipo, int cantidadDeVentas)
        {
            switch (tipo)
            {
                case Tipo.Gerente:
                    return 150000;
                case Tipo.Supervisor:
                    return 60000;
                case Tipo.Secretaria:
                    return 15000;
                case Tipo.Administrativo:
                    return 20000;
                case Tipo.Vendedor:
                    return 30000;
                case Tipo.VendedorPorComision:
                    return 30000 + (valorPorVenta * cantidadDeVentas);
                default:
                    throw new ApplicationException("No existe el tipo de empleado");
            }
        }   
    }

    class Empleado
    {
        Tipo tipo;   
        
        public Empleado(Tipo tipo)
        {
            this.tipo = tipo;
        }
    }

    enum Tipo
    {
        Gerente,
        Supervisor,
        Secretaria,
        Administrativo,
        Vendedor,
        VendedorPorComision
    }
}
