﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadorDeSueldos2
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculadora calculadora = new Calculadora();

            Empleado gerente = new Gerente();
            Console.WriteLine("El {0} gana: ", gerente.GetType().Name);
            Console.WriteLine(calculadora.ObtenerSueldo(gerente));

            Empleado vendedorPorComision = new VendedorPorComision(10);
            Console.WriteLine("El {0} gana: ", vendedorPorComision.GetType().Name);
            Console.WriteLine(calculadora.ObtenerSueldo(vendedorPorComision));
        }
    }

    class Calculadora
    {
        public double ObtenerSueldo(Empleado empleado)
        {
            return empleado.ObtenerSueldo();
        }
    }

    abstract class Empleado
    {
        public abstract double ObtenerSueldo();
    }

    class Gerente : Empleado
    {
        public override double ObtenerSueldo()
        {
            return 150000;
        }
    }

    class Supervisor : Empleado
    {
        public override double ObtenerSueldo()
        {
            return 60000;
        }
    }

    class Secretaria : Empleado
    {
        public override double ObtenerSueldo()
        {
            return 15000;
        }
    }

    class Administrativo : Empleado
    {
        public override double ObtenerSueldo()
        {
            return 20000;
        }
    }

    class Vendedor : Empleado
    {
        public override double ObtenerSueldo()
        {
            return 30000;
        }
    }

    class VendedorPorComision : Vendedor
    {
        int cantidadDeVentas;
        double valorPorVenta = 1000;

        public VendedorPorComision(int cantidadDeVentas)
        {
            this.cantidadDeVentas = cantidadDeVentas;
        }

        public override double ObtenerSueldo()
        {
            return base.ObtenerSueldo() + (cantidadDeVentas * valorPorVenta);
        }
    }
}
