package com.company;

import java.util.ArrayList;
import java.util.Random;

class Main {

    public static void main(String[] args) {

        Server server = LoadBalancer
                .getInstance()
                .getServer();

        System.out.println(server.getAddress());
    }
}

class LoadBalancer {

    private final ArrayList<Server> servers = new ArrayList<>();
    private static LoadBalancer instance = null;
    private static final Object padlock = new Object();

    private LoadBalancer() {

        this.servers.add(new Server("192.168.0.1"));
        this.servers.add(new Server("192.168.0.2"));
        this.servers.add(new Server("192.168.0.3"));
        this.servers.add(new Server("192.168.0.4"));
        this.servers.add(new Server("192.168.0.5"));
    }

    public Server getServer() {
        Random random = new Random();
        int i = random.nextInt(servers.size());
        return servers.get(i);
    }

    public static LoadBalancer getInstance() {
        if (instance == null) {
            synchronized (padlock) {
                if (instance == null)
                    instance = new LoadBalancer();
            }
        }
        return instance;
    }
}

class Server {
    private final String address;

    public Server(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
