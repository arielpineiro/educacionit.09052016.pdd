﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema7
{
    class Program
    {
        static void Main(string[] args)
        {
            Color rojo = new Color(255, 0, 0);
            Color azul = new Color(0, 0, 255);

            Color otroColorRojo = rojo.Clonar() as Color;

            if (rojo != otroColorRojo)
                Console.WriteLine("Son clones. ");
            
        }
    }

    abstract class ObjetoClonable
    {
        public abstract ObjetoClonable Clonar();
    }

    class Color : ObjetoClonable
    {
        int rojo;
        int verde;
        int azul;
        // Referencia referencia;

        public Color(int rojo, int verde, int azul)
        {
            this.rojo = rojo;
            this.verde = verde;
            this.azul = azul;
        }

        public override ObjetoClonable Clonar()
        {
            return MemberwiseClone() as ObjetoClonable;
        }
    }
}
