﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema6
{
    class Program
    {
        static void Main(string[] args)
        {
            Accion accion = new Accion("GOOG", 100.0);

            Interesado interesado1 = new Inversor("Jorge Sanchez");
            Interesado interesado2 = new Inversor("Daniel Osvaldo");

            accion.Agregar(interesado1);
            accion.Agregar(interesado2);

            accion.Precio = 100.1;
            accion.Precio = 99.9;
            accion.Precio = 99.7;
        }
    }

    abstract class Interesado
    {
        public abstract void Actualizar(Accion accion);
    }

    class Inversor : Interesado
    {
        private Accion accion;
        private string nombre;

        public Inversor(string nombre)
        {
            this.nombre = nombre;
        }

        public override void Actualizar(Accion accion)
        {
            this.accion = accion;

            Console.WriteLine("Hola soy {0} y estoy del nuevo valor {1} en {2}", nombre, accion.Nombre, accion.Precio);
        }
    }

    class Accion
    {
        private string nombre;
        private double precio;
        private List<Interesado> interesados = new List<Interesado>();

        public string Nombre
        {
            get { return nombre; }
        }

        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
                Notificar();
            }
        }

        private void Notificar()
        {
            foreach (Interesado interesado in interesados)
            {
                interesado.Actualizar(this);
            }
        }

        public Accion(string nombre, double precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void Agregar(Interesado interesado)
        {
            interesados.Add(interesado);
        }

        public void Remover(Interesado interesado)
        {
            interesados.Remove(interesado);
        }
    }
}
