﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Referencias
{
    class Program
    {
        static void Main(string[] args)
        {
            B b = new B();
            A a = new A(b);            
        }
    }

    class A
    {
        public A(B b)
        {
            b.Foo(this);
        }
    }

    class B
    {
        public void Foo(A a)
        {
            Console.WriteLine("Hola soy {0} ayudado por {1}", GetType().Name, a.GetType().Name);            
        }
    }
}
