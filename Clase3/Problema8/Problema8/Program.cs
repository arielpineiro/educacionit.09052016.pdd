﻿using System;

namespace Problema8
{
    class Program
    {
        private static void Main(string[] args)
        {
            Orden orden = new Orden { Monto = 1000 };
            Instante instante = orden.CrearInstante();

            try
            {
                orden.Monto = 1500;
                throw new ApplicationException();
            }
            catch (Exception)
            {
                orden.Recuperar(instante);
            }
            Console.WriteLine(orden.Monto);
        }
    }

    class Orden
    {
        public double Monto { get; set; }

        public Instante CrearInstante()
        {
            return new Instante(Monto);
        }

        public void Recuperar(Instante instante)
        {
            Monto = instante.Monto;
        }
    }

    class Instante
    {
        public double Monto { get; set; }

        public Instante(double monto)
        {
            Monto = monto;
        }
    }
}