﻿using System;
using System.Collections.Generic;

namespace Problema5
{
    class Program
    {
        static void Main(string[] args)
        {
            SalaDeChat salaDeChat = new SalaDeChatDeWebex();

            Participante Juan = new Participante("Juan");
            Participante Maria = new Participante("Maria");

            salaDeChat.Registrar(Juan);
            salaDeChat.Registrar(Maria);

            Juan.Enviar("Maria", "Hola, cómo estás?");
            Maria.Enviar("Juan", "Bien y vos?");

        }
    }

    abstract class SalaDeChat
    {
        public abstract void Registrar(Participante participante);
        public abstract void Enviar(string de, string para, string mensaje);
    }

    class SalaDeChatDeWebex : SalaDeChat
    {
        Dictionary<string, Participante> participantes = new Dictionary<string, Participante>();
        public override void Enviar(string de, string para, string mensaje)
        {
            Participante participante = participantes[para];

            if (participante != null)
                participante.Recibir(de, mensaje);
        }

        public override void Registrar(Participante participante)
        {
            if (!participantes.ContainsKey(participante.Nombre))
                participantes.Add(participante.Nombre, participante);

            participante.SalaDeChat = this;
        }
    }

    class Participante
    {
        private string nombre;

        public SalaDeChat SalaDeChat { get; set; }

        public string Nombre
        {
            get { return nombre;}
        }

        public Participante(string nombre)
        {
            this.nombre = nombre;
        }

        public void Enviar(string para, string mensaje)
        {
            SalaDeChat.Enviar(nombre, para, mensaje);
        }

        public void Recibir(string de, string mensaje)
        {
            Console.WriteLine("{0} para {1}: '{2}'", de, nombre, mensaje);
        }
    }
}
