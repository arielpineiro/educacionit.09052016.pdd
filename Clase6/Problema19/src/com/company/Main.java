package com.company;

public class Main {

    public static void main(String[] args) {
        PaymentFacade paymentFacade = new PaymentFacade();
        paymentFacade.payment(123);;
    }
}

class PaymentFacade {
    SystemA systemA;
    SystemB systemB;
    SystemC systemC;

    public void payment(Integer id) {
        systemA.a();
        systemB.b();
        systemC.c();
    }
}

class SystemA {
    public void a() {}
}

class SystemB {
    public void b() {}
}

class SystemC {
    public void c() {}
}

class SystemAAdapter {

}