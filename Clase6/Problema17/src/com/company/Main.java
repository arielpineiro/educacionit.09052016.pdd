package com.company;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
	    Action action = new Accessory(new Web(new Log(new Cache(new Transactional(new Method("getCustomerById()"))))));
        action.execute();
    }
}

abstract class Action {
    public abstract void execute();
}

class Method extends Action {

    private final String name;

    Method(String name) {
        this.name = name;
    }

    @Override
    public void execute() {
        out.println(this.name);
    }
}

class Accessory extends Action {

    private final Action action;

    Accessory(Action action ){
        this.action = action;
    }

    @Override
    public void execute() {
        out.println("[" + action.getClass().getSimpleName() + "]");
        this.action.execute();
    }
}

class Log extends Accessory {

    Log(Action action) {
        super(action);
    }
}

class Transactional extends Accessory {

    Transactional(Action action) {
        super(action);
    }
}

class Cache extends Accessory {

    Cache(Action action) {
        super(action);
    }
}

class Web extends Accessory {

    Web(Action action) {
        super(action);
    }
}