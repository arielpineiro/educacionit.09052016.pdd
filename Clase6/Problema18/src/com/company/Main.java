package com.company;

import java.util.ArrayList;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
        Composite raiz = new Composite("raiz");

        Simple s1 =  new Simple("s1");
        Simple s2 =  new Simple("s2");

        Composite c2 = new Composite("c2");
        Simple s3 = new Simple("s3");
        c2.add(s3);

        raiz.add(s1);
        raiz.add(s2);
        raiz.add(c2);

        raiz.show();
    }
}


abstract class Component {
    String name;

    Component(String name) {
        this.name = name;
    }

    public abstract void show();
}

class Simple extends Component {

    Simple(String name) {
        super(name);
    }

    @Override
    public void show() {
        out.println(this.name);
    }
}

class Composite extends Component {

    ArrayList<Component> components = new ArrayList<>();

    Composite(String name) {
        super(name);
    }

    public void add(Component component) {
        this.components.add(component);
    }

    @Override
    public void show() {
        out.println(this.name);
        for (Component component : components){
            component.show();
        }
    }
}

