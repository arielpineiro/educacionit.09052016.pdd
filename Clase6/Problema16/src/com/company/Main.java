package com.company;

import java.util.HashMap;
import java.util.HashSet;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
	    //HttpRequest request = new HttpRequest();

        // Instanciación sencilla pero poco práctica.
        //request.setMethod("POST");
        //request.setMethod(HttpVerb.POST);
        //request.setBody("name=jorge");
        //request.addHeader("Content-Type: application/json");
        //request.setUri("https//api.facebook.com/users/123/message");

        HttpRequest request = new HttpRequest()
                .isPost()
                .setBody("name=jorge")
                .setHeader(new Header("Content-Type: application/json"))
                .setUri("https//api.facebook.com/users/123/message")
                .getHttpRequest();

        out.println(request);

        HttpRequestBase httpRequest = new HttpRequestPost();
    }
}

abstract class HttpRequestBase {
    protected HashMap<String, String> params = new HashMap<>();
    protected String method;
    protected String uri;
    protected String body;
    protected HashSet<Header> headers = new HashSet<>();

    public abstract void setMethod();
}

class HttpRequestPost extends HttpRequestBase {

    @Override
    public void setMethod() {
        this.method = "POST";
    }
}

class HttpRequest {
    HashMap<String, String> params = new HashMap<>();
    String method;
    String uri;
    String body;
    HashSet<Header> headers = new HashSet<>();

    public HttpRequest isPost() {
        this.method = "POST";
        return this;
    }

    public HttpRequest isGet() {
        this.method = "GET";
        return this;
    }

    public HttpRequest setBody(String body) {
        this.body = body;
        return this;
    }

    public HttpRequest setHeader(Header header) {
        this.headers.add(header);
        return this;
    }

    public HttpRequest setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public HttpRequest getHttpRequest() {
        return this;
    }

    @Override
    public String toString() {
        return "Method: " + this.method + " , Uri: " + this.uri;
    }
}

class Header {
    String name;

    public Header(String name) {
        this.name = name;
    }
}
