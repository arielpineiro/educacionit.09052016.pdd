package com.company;

public class Main {

    public static void main(String[] args) {
	    Vehicle vehicle = new Car();
        vehicle.travel();

    }
}

abstract class Vehicle {

    protected abstract void startEngine();
    protected abstract void accelerate();
    protected abstract void stopEngine();

    public void checkFuel() {
        System.out.println("Checking fuel...");
    }

    // Método plantilla ó Template Method
    public void travel() {
        checkFuel();
        startEngine();
        accelerate();
        stopEngine();
    }

    //protected abstract void m4();

    public void move(){
        //m1();
        //m2();
        //m3();
        //m4(); es especifico del tipo
        //m5();
        //m6();
        //m7();
    }
}

class Car extends Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Starting..." + this.getClass().getSimpleName());
    }

    @Override
    public void accelerate() {
        System.out.println("Moving..." + this.getClass().getSimpleName());
    }

    @Override
    public void stopEngine() {
        System.out.println("Stopping..." + this.getClass().getSimpleName());
    }
}

class Motocycle extends Vehicle {

    @Override
    public void startEngine() {

    }

    @Override
    public void accelerate() {

    }

    @Override
    public void stopEngine() {

    }
}

class Truck extends Vehicle
{

    @Override
    public void startEngine() {

    }

    @Override
    public void accelerate() {

    }

    @Override
    public void stopEngine() {

    }
}

class AirPlane extends Vehicle {

    @Override
    protected void startEngine() {
        // trhow new notimplemetedexception
    }

    @Override
    protected void accelerate() {

    }

    @Override
    protected void stopEngine() {

    }
}
