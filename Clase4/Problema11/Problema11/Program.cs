﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema11
{
    class Program
    {
        static void Main(string[] args)
        {
            Protocol protocol = new WifiProtocol();
            Message message = new SMSMessage(protocol);
            message.Send("Hola mundo!");
        }
    }

    abstract class Message
    {
        protected Protocol protocol;

        public Message(Protocol protocol)
        {
            this.protocol = protocol;
        }

        public void Send(string text)
        {
            Console.WriteLine("Sending " + text + " ...");
            protocol.Send(text);
        }
    }

    class SMSMessage : Message
    {
        public SMSMessage(Protocol protocol) : base(protocol)
        {
        }
    }

    class MMSMessage : Message
    {
        public MMSMessage(Protocol protocol) : base(protocol)
        {
        }
    }

    abstract class Protocol
    {
        public abstract void Send(string text);
    }

    class WifiProtocol : Protocol
    {
        public override void Send(string text)
        {
            Console.WriteLine("Wifi sending...");
        }
    }

    class FourGProtocol : Protocol
    {
        public override void Send(string text)
        {
            Console.WriteLine("4G sending...");
        }
    }
}
