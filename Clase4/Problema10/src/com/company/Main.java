package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        IShell shell = new WindowsShell();

        BackupCommand backupCommand = new BackupCommand(shell);
        ZipCommand zipCommand = new ZipCommand(shell);

        ArrayList<ICommand> commands = new ArrayList<>();
        commands.add(backupCommand);
        commands.add(zipCommand);

        Invoker invoker = new Invoker(commands, new ParallelMode());
        invoker.execute();
    }
}

interface ICommand {
    void execute();
}

interface IShell {
    void copy();
    void list();
    void zip();
}

class Invoker implements ICommand {

    private final ArrayList<ICommand> commands;
    private ExecutionMode executionMode = new SerialMode();

    Invoker (ArrayList<ICommand> commands) {
        this.commands = commands;
    }

    Invoker (ArrayList<ICommand> commands, ExecutionMode executionMode) {
        this.commands = commands;
        this.executionMode = executionMode;
    }

    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    @Override
    public void execute() {
        executionMode.run(commands);
    }
}

abstract class ExecutionMode {
    public abstract void run(ArrayList<ICommand> commands);
}

class SerialMode  extends ExecutionMode{

    @Override
    public void run(ArrayList<ICommand> commands) {

        commands.stream().forEach(x-> x.execute());
    }
}

class ParallelMode extends ExecutionMode {

    @Override
    public void run(ArrayList<ICommand> commands) {

        commands.parallelStream().forEach(x-> x.execute());
    }
}


class WindowsShell implements IShell {

    @Override
    public void copy() {

    }

    @Override
    public void list() {

    }

    @Override
    public void zip() {

    }
}

class ZipCommand implements ICommand {

    private final IShell shell;

    ZipCommand(IShell shell) {
        this.shell = shell;
    }

    @Override
    public void execute() {

    }
}

class BackupCommand implements ICommand {

    private final IShell shell;

    BackupCommand(IShell shell) {
        this.shell = shell;
    }

    @Override
    public void execute() {

    }
}
