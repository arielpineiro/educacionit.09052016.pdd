﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema12
{
    class Program
    {
        static void Main(string[] args)
        {
            IPayment payment = new DineroMailPayment();
            payment.Pay();
        }
    }

    interface IPayment
    {
        void Pay();
    }

    class DineroMailPayment : IPayment
    {
        DineroMail dineroMail = new DineroMail();

        public void Pay()
        {
            dineroMail.PagarOrden();
        }
    }

    class PayPalPayment : IPayment
    {
        Paypal paypal = new Paypal();
        public void Pay()
        {
            paypal.PayTransaction();
        }
    }

    class DineroMail
    {
        public void PagarOrden()
        {

        }
    }

    class Paypal
    {
        public void PayTransaction()
        {

        }
    }
}
