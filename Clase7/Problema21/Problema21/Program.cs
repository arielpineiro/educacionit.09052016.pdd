﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Problema21
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			IEnumerable<int> values = new List<int> { 10, 12, 18, 20, 32 };

			foreach (var item in values)
			{
				//Console.WriteLine(item);
			}

			IEnumerator<int> enumerator = values.GetEnumerator();

			while (enumerator.MoveNext())
			{
				int value = enumerator.Current;
				Console.WriteLine(value);
			}

			DoSomething(values);
		}

		static void DoSomething(IEnumerable<int> values)
		{
			throw new NotImplementedException();
		}
}

	class MyCollection<T> : IEnumerable<T>
	{
		public IEnumerator<T> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}
	}
}

