﻿using System;
using System.Collections.Generic;

namespace Problema22
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Structure structure = new Structure();
			structure.Add(new ConcreteElement());
			structure.Add(new ConcreteElement());
			structure.Add(new ConcreteElement());
			structure.Add(new ConcreteElement());

			structure.Accept(new ConcreteVisitorA());
			structure.Accept(new ConcreteVisitorB());

		}
	}

	interface Element
	{
		void Accept(Visitor visitor);
	}

	class ConcreteElement : Element
	{
		public void Accept(Visitor visitor)
		{
			visitor.Visit(this);
		}
	}

	interface Visitor
	{
		void Visit(Element element);
	}

	class ConcreteVisitorA : Visitor
	{
		public void Visit(Element element)
		{
			Console.WriteLine("Visitor A");
		}
	}

	class ConcreteVisitorB : Visitor
	{
		public void Visit(Element element)
		{
			Console.WriteLine("Visitor B");
		}
	}

	class Structure
	{
		List<Element> elements = new List<Element>();

		public void Add(Element element)
		{
			this.elements.Add(element);
		}

		public void Accept(Visitor visitor)
		{
			foreach (Element element in elements)
			{
				element.Accept(visitor);
			}
		}			
	}
}
