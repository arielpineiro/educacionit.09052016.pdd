package com.company;

import com.sun.tools.javac.util.Convert;

import java.util.ArrayList;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        // Convertir un número binario a decimal
        // Convertir un número romano a decimal
        // Convertir una sentencia SQL en algo admitido por el motor.
        // Realizar una calcudora con operaciones basicas de suma y resta para un número en notación polaca inversa.

        // 10 + 5 - 2
        // 20 - 1 - 3 + 4
        // (20 * 1) - (3 * 4)
        String operacion = "20 1 - 3 - 4 +";

        Parser parser = new Parser(operacion);
        Integer resultado = parser.evaluar();
        System.out.println(resultado);
    }

}


interface Expresion {
    void interpretar(Stack<Integer> pila);
}

class ExpresionTerminalSuma implements Expresion {

    @Override
    public void interpretar(Stack<Integer> pila) {

        pila.push(pila.pop() + pila.pop());
    }
}

class ExpresionTerminalResta implements Expresion {

    @Override
    public void interpretar(Stack<Integer> pila) {

        int temporal = pila.pop();
        pila.push(pila.pop() - temporal);
    }
}

class ExpresionTerminalNumero implements Expresion {

    private final Integer numero;

    ExpresionTerminalNumero (Integer numero) {
        this.numero = numero;
    }

    @Override
    public void interpretar(Stack<Integer> pila) {
        pila.push(this.numero);
    }
}

class Parser {

    ArrayList<Expresion> arbol = new ArrayList<>();

    Parser(String operacion) {
        for (String segmento : operacion.split(" ")) {
            switch (segmento) {
                case "+" : arbol.add(new ExpresionTerminalSuma()); break;
                case "-" : arbol.add(new ExpresionTerminalResta()); break;
                default:
                    arbol.add(new ExpresionTerminalNumero(Integer.parseInt(segmento)));
            }
        }
    }

    public Integer evaluar() {
        Stack<Integer> pila = new Stack<>();

        for (Expresion expresion : arbol) {
            expresion.interpretar(pila);
        }

        return pila.pop();
    }
}
