﻿using System;
using System.Collections.Generic;

namespace Problema23
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			string documento = "HELLO";

			char[] caracteres = documento.ToCharArray();

			// Estado extrinseco
			int tamanio = 10;

			FabricaDeCaracteres fabrica = new FabricaDeCaracteres();

			foreach (var c in caracteres)
			{
				Caracter caracter = fabrica.DameCaracter(c);
				caracter.Mostrar(tamanio);
			}
		}
	}


	class FabricaDeCaracteres
	{
		Dictionary<char, Caracter> caracteres = new Dictionary<char, Caracter>();


		public Caracter DameCaracter(char key)
		{
			Caracter caracter = null;

			if (!caracteres.ContainsKey(key))
			{
				switch (key)
				{
					case 'H': caracter = new H(); break;
					case 'E': caracter = new E(); break;
					case 'L': caracter = new L(); break;
					case 'O': caracter = new O(); break;
					default:
						break;
				}
				caracteres.Add(key, caracter);
			}

			return caracteres[key];

		}
	}

	abstract class Caracter
	{
		// Estado intrinseco
		protected char simbolo;
		protected int ancho;
		protected int alto;

		public abstract void Mostrar(int tamanio);
	}

	class H : Caracter
	{
		public H()
		{
			this.simbolo = 'H';
			this.ancho = 100;
			this.alto = 40;
		}

		public override void Mostrar(int tamanio)
		{
			Console.WriteLine(this.GetType().Name);
		}
	}

	class E : Caracter
	{
		public E()
		{
			this.simbolo = 'E';
			this.ancho = 100;
			this.alto = 40;
		}

		public override void Mostrar(int tamanio)
		{
			Console.WriteLine(this.GetType().Name);
		}
	}

	class L : Caracter
	{
		public L()
		{
			this.simbolo = 'L';
			this.ancho = 100;
			this.alto = 40;
		}
		public override void Mostrar(int tamanio)
		{
			Console.WriteLine(this.GetType().Name);
		}
	}

	class O : Caracter
	{
		public O()
		{
			this.simbolo = 'H';
			this.ancho = 100;
			this.alto = 40;
		}

		public override void Mostrar(int tamanio)
		{
			Console.WriteLine(this.GetType().Name);
		}
	}
}
