﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema3
{
    class Program
    {
        static void Main(string[] args)
        {
            IShell shell = new Bash();

            List<ITask> tasks = new List<ITask>();

            // Crear un backup
            ITask backupTask = new BackupTask(shell);
            tasks.Add(backupTask);
            
            // Hacer un restore
            ITask restoreTask = new RestoreTask(shell);
            tasks.Add(restoreTask);

            ITask batchTask = new BatchTask(tasks);
            batchTask.Execute();
        }
    }

    
    interface ITask
    {
        void Execute();
    }

    class BatchTask : ITask
    {
        private List<ITask> tasks;

        public BatchTask(List<ITask> tasks)
        {
            this.tasks = tasks;
        }
        
        public void Execute()
        {
            Parallel
                .ForEach(tasks, x => x.Execute());            
        }
    }

    class BackupTask : ITask
    {
        private IShell shell;

        public BackupTask(IShell shell)
        {
            this.shell = shell;
        }
        public void Execute()
        {
            // Crear un backup
            shell.Zip();
            shell.Move();
        }
    }

    class RestoreTask : ITask
    {
        private IShell shell;

        public RestoreTask(IShell shell)
        {
            this.shell = shell;
        }
        public void Execute()
        {
            // Hacer un restore
            shell.Move();
            shell.UnZip();
        }
    }

    interface IShell
    {
        void Copy();
        void Move();
        void Zip();
        void UnZip();
    }

    class Dos : IShell
    {
        public void Copy()
        {
            throw new NotImplementedException();
        }

        public void Move()
        {
            throw new NotImplementedException();
        }

        public void UnZip()
        {
            throw new NotImplementedException();
        }

        public void Zip()
        {
            throw new NotImplementedException();
        }
    }

    class Bash : IShell
    {
        public void Copy()
        {
            throw new NotImplementedException();
        }

        public void Move()
        {
            Console.WriteLine("Moving files...");
        }

        public void UnZip()
        {
            Console.WriteLine("Unzipping files...");
        }

        public void Zip()
        {
            Console.WriteLine("Zipping files...");
        }
    }
}
