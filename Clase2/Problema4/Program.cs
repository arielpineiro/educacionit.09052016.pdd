﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema4
{
    class Program
    {
        static void Main(string[] args)
        {
            Aprobador empleado = new Empleado();
            Aprobador supervisor = new Supervisor();
            Aprobador gerente = new Gerente();

            empleado.Siguiente(supervisor);
            supervisor.Siguiente(gerente);

            empleado.AtenderSolicitud(1000000);
        }
    }
    
    abstract class Aprobador
    {
        protected Aprobador siguiente;

        public void Siguiente(Aprobador aprobador)
        {
            siguiente = aprobador;
        }

        public abstract void AtenderSolicitud(double monto);
    }

    class Empleado : Aprobador
    {
        public override void AtenderSolicitud(double monto)
        {
            if (monto < 20000)
            {
                Console.WriteLine("Soy {0}, prestamo otogardo", this.GetType().Name);
            }
            else
            {
                siguiente.AtenderSolicitud(monto);
            }
        }
    }

    class Supervisor : Aprobador
    {
        public override void AtenderSolicitud(double monto)
        {
            if (monto < 100000)
            {
                Console.WriteLine("Soy {0}, prestamo otogardo", this.GetType().Name);
            }
            else
            {
                siguiente.AtenderSolicitud(monto);
            }
        }
    }

    class Gerente : Aprobador
    {
        public override void AtenderSolicitud(double monto)
        {
            if (monto < 500000)
            {
                Console.WriteLine("Soy {0}, prestamo otogardo", this.GetType().Name);
            }
            else
            {
                throw new ApplicationException("Llamar a directorio!!!");
            }
        }
    }
}
