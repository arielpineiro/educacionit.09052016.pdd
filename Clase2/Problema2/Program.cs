﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Program
    {
        static void Main(string[] args)
        {
            ICalculadora calculadora = new ProxyCalculadora();

            double a = 10;
            double b = 12;

            double resultado = calculadora.Sumar(a, b);

            Console.WriteLine(resultado);
        }
    }

    interface ICalculadora
    {
        double Sumar(double a, double b);
        double Restar(double a, double b);
        double Multiplicar(double a, double b);
        double Dividir(double a, double b);
    }

    class Calculadora : ICalculadora
    {
        public double Dividir(double a, double b)
        {
            return a / b;
        }

        public double Multiplicar(double a, double b)
        {
            return a * b ;
        }

        public double Restar(double a, double b)
        {
            return a - b;
        }

        public double Sumar(double a, double b)
        {
            return a + b;
        }
    }

    class ProxyCalculadora : ICalculadora
    {
        ICalculadora calculadora = new Calculadora();

        public double Dividir(double a, double b)
        {
            return calculadora.Dividir(a, b);
        }

        public double Multiplicar(double a, double b)
        {
            return calculadora.Multiplicar(a, b);
        }

        public double Restar(double a, double b)
        {
            return calculadora.Restar(a, b);
        }

        public double Sumar(double a, double b)
        {
            return calculadora.Sumar(a, b);
        }
    }
}
