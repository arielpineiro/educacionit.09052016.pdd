﻿using EjemploWSDL.WeatherService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploWSDL
{
    class Program
    {
        static void Main(string[] args)
        {
            WeatherSoapClient client = new WeatherSoapClient();

            WeatherDescription[] results = client.GetWeatherInformation();

            foreach (var item in results)
            {
                Console.WriteLine(item.Description);
            }
        }
    }
}
