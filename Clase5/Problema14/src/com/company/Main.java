package com.company;

public class Main {

    public static void main(String[] args) {
        CreadorDeMundo mundo = new CreadorDeTierra();

        ManejadorDeMundos manejador = new ManejadorDeMundos(mundo);
        manejador.dialogar();

        //SqlServerDaoFactory daoFactory = new SqlServerDaoFactory();
        //SqlServerDaoFactory daoFactory = new OracleDaoFactory();
        //ICustomerDao customerDao = daoFactory.getCustomerDao();
        //customerDao.Execute("SELECT * FROM Table");

        //IRepository repository = new RedisRepository();
        //IRepository repository = new MemcachedRepository();
        //repository.Add("key", "value");
    }
}

class ManejadorDeMundos {

    private final CreadorDeMundo creadorDeMundo;

    ManejadorDeMundos(CreadorDeMundo creadorDeMundo) {
        this.creadorDeMundo = creadorDeMundo;
    }

    public void dialogar() {
        Dios dios = this.creadorDeMundo.crearDios();
        Representante representante = this.creadorDeMundo.crearRepresentante();
        dios.solicita(representante);
    }
}

abstract class CreadorDeMundo {
    public abstract Dios crearDios();
    public abstract Representante crearRepresentante();
}

class CreadorDeInframundo extends  CreadorDeMundo {

    @Override
    public Dios crearDios() {
        return new Hades();
    }

    @Override
    public Representante crearRepresentante() {
        return new Espectro();
    }
}

class CreadorDeTierra extends CreadorDeMundo {

    @Override
    public Dios crearDios() {
        return new Athena();
    }

    @Override
    public Representante crearRepresentante() {
        return new Caballero();
    }
}

class Dios {
    public void solicita(Representante representante) {
        System.out.println(this.getClass().getSimpleName() + " ordena a " + representante.getClass().getSimpleName());
    }
}

class Poseidon extends Dios {

}

class Athena extends Dios {

}

class Zeus extends Dios {

}

class Hades extends Dios {

}

class Representante {

}

class Caballero extends Representante {

}

class Espectro extends Representante {

}

class Marine extends Representante {

}

class Angel extends Representante {

}

