package com.company;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) throws Exception {
	    // Inicialmente en estado líquido.
        Sustancia sustancia = new Agua(new Liquido(20));
        out.println(sustancia.getEstado());

        // Pasar a estado gaseoso.
        sustancia.subirTemperatura(10);
        sustancia.subirTemperatura(100);
        out.println(sustancia.getEstado());

        // Pasar a estado solido.
        sustancia.disminuirTemperatura(-100);
        sustancia.disminuirTemperatura(-50);
        out.println(sustancia.getEstado());
    }
}

abstract class Sustancia {
    Estado estado;

    Sustancia (Estado estado) {
        this.estado = estado;
    }

    public void subirTemperatura(double grados) throws Exception {
        this.estado.subirTemperatura(grados);
    };

    public void disminuirTemperatura(double grados) throws Exception {
        this.estado.disminuirTemperatura(grados);
    };

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}

class Agua extends Sustancia {

    Agua(Estado estado) {
        super(estado);
        this.estado.setSustancia(this);
    }
}

abstract class Estado {
    protected double temperatura;
    protected double limiteInferior;
    protected double limiteSuperior;
    protected Sustancia sustancia;

    Estado (double temperatura) {
        this.temperatura = temperatura;
        definirLimites();
    }

    Estado(Estado estado) {
        this(estado.temperatura, estado.sustancia);
    }

    public Estado(double temperatura, Sustancia sustancia) {
        this.temperatura = temperatura;
        this.sustancia = sustancia;
        definirLimites();
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    public void subirTemperatura(double grados) throws Exception {
        if (grados > 0) {
            this.temperatura = this.temperatura + grados;
            verificarCambioDeEstado();
        }
    }

    public void disminuirTemperatura(double grados) throws Exception {
        if (grados <= 0) {
            this.temperatura = this.temperatura + grados;
            verificarCambioDeEstado();
        }
    };

    public void setSustancia(Sustancia sustancia) {
        this.sustancia = sustancia;
    }

    public abstract void definirLimites();
    protected abstract void verificarCambioDeEstado() throws Exception;
}

class Solido extends Estado {

    Solido(double temperatura) {
        super(temperatura);
    }

    public Solido(Estado estado) {
        super(estado);
    }

    @Override
    public void definirLimites() {
        this.limiteInferior = -273;
        this.limiteSuperior = 0;
    }

    @Override
    protected void verificarCambioDeEstado() throws Exception {
        if (temperatura > limiteSuperior) {
            sustancia.setEstado(new Liquido(this));
        }
        if (temperatura < limiteInferior) {
            throw new Exception();
        }
    }
}

class Liquido extends Estado {

    Liquido(double temperatura) {
        super(temperatura);
    }

    Liquido(Estado estado) {
        super(estado);
    }

    @Override
    public void definirLimites() {
        this.limiteInferior = 0;
        this.limiteSuperior = 80;
    }

    @Override
    protected void verificarCambioDeEstado() {
        if (temperatura > limiteSuperior) {
            sustancia.setEstado(new Gaseoso(this));
        }
        if (temperatura < limiteInferior) {
            sustancia.setEstado(new Solido(this));
        }
    }
}

class Gaseoso extends Estado {

    Gaseoso(double temperatura) {
        super(temperatura);
    }

    public Gaseoso(Estado estado) {
        super(estado);
    }

    @Override
    public void definirLimites() {
        this.limiteInferior = 80;
        this.limiteSuperior = 3300;
    }

    @Override
    protected void verificarCambioDeEstado() throws Exception {
        if (temperatura > limiteSuperior) {
            throw new Exception();
        }
        if (temperatura < limiteInferior) {
            sustancia.setEstado(new Liquido(this));
        }
    }
}