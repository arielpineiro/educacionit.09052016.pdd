package com.company;

import javax.print.Doc;
import java.util.ArrayList;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {

        Documento documento = new Curriculum();

        for (Pagina pagina : documento.getPaginas()) {
            out.println(pagina.getClass().getSimpleName());
        }
    }

}

abstract class Documento {
    protected ArrayList<Pagina> paginas = new ArrayList<>();

    Documento() {
        crearPaginas();
    }

    public ArrayList<Pagina> getPaginas() {
        return paginas;
    }

    // Factory Method
    public abstract void crearPaginas();
}

class Pagina {

}


class Curriculum extends Documento {

    @Override
    public void crearPaginas() {
        this.paginas.add(new ExperienciaLaboral());
        this.paginas.add(new Introduccion());
        this.paginas.add(new Educacion());
    }
}

class Tesis extends Documento {

    @Override
    public void crearPaginas() {
        this.paginas.add(new Conclusion());
        this.paginas.add(new Desarrollo());
        this.paginas.add(new Abstract());
        this.paginas.add(new Bibliografia());
    }
}

class ExperienciaLaboral extends Pagina {

}

class Introduccion extends Pagina {

}

class Educacion extends Pagina {

}

class Conclusion extends Pagina {

}

class Desarrollo extends Pagina {

}

class Bibliografia extends Pagina {

}

class Abstract extends Pagina {

}